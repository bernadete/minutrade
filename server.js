/* eslint-disable no-console */

var cors = require('cors')
var bodyParser = require('body-parser')
var express = require('express')
var productsController = require('./products-controller')

var app = express()

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.get('/products', function(req, res) {
	productsController.get(req, res)
})
app.post('/products', function(req, res) {
	productsController.post(req, res)
})
app.put('/products/name/:name', function(req, res) {
	productsController.put(req, res)
})
app.delete('/products/name/:name', function(req, res) {
	productsController.del(req, res)
})

var server = app.listen(3000, function () {
	console.log('Listening on port %s', server.address().port)
})
