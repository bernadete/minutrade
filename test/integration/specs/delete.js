
/*global request*/
/*eslint no-undef: "error"*/

const dataProdutos = require('../data/products.js')

describe('Exclusão', () => {

	before('Inserindo novo produto', done => {
		request
			.post('/')
			.send(dataProdutos.novoProdutoDel)
			.expect(201)
		done()
	})

	it('Exclusão de produto - Status 201', done => {
		request
			.del('/name/produtoTeste3')
			.expect(201)
		done()
	})
})
