
/*global request, expect*/
/*eslint no-undef: "error"*/

describe('Listagem de produtos', () => {
	it('Obter produtos - Status 200', done => {
		request
			.get('/')
			.expect(200)
			.end((err, res) => {
				expect(res.body).to.be.an('array')
				done(err)
			})
	})
})
