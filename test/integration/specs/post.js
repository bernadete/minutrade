
/*global request, expect*/
/*eslint no-undef: "error"*/


const dataProdutos = require('../data/products.js')

describe('Cadastro de Produto - Sucesso', () => {
	it('Cadastro de novo produto - Status 201', done => {
		request
			.post('/')
			.send(dataProdutos.novoProduto)
			.expect(201)
			.end((err, res) => {
				expect(res.body.name).to.equal(dataProdutos.novoProduto.name)
				expect(res.body).to.be.an('object')
				done(err)
			})
	})

	it('Cadastro de novo produto sem nome - Status 201', done => {
		request
			.post('/')
			.send(dataProdutos.produtoSemNome)
			.expect(201)
			.end((err, res) => {
				expect(res.body).to.be.an('object')
				done(err)
			})
	})
})
