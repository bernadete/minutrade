/*global request*/
/*eslint no-undef: "error"*/

const joiAssert = require('joi-assert')
const dataProdutos = require('../data/products.js')
const {
	schemaPayload
} = require('../schema/schema.js')


describe('Validando schema', () => {
	it('Payload', done => {
		request
			.post('/')
			.send(dataProdutos.novoProduto)
			.expect(201)
			.end((err) => {
				joiAssert(dataProdutos.novoProdutoSchema, schemaPayload)
				done(err)
			})
	})
})
