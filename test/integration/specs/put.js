/*global request, expect*/
/*eslint no-undef: "error"*/


const dataProdutos = require('../data/products.js')

describe('Alteração produto', () => {

	// teste vai falhar
	before('Inserindo novo produto', done => {
		request
			.post('/')
			.send(dataProdutos.novoProdutoPut)
			.expect(201)
			.end((err, res) => {
				expect(res.body.name).to.equal(dataProdutos.novoProdutoPut.name)
				done(err)
			})
	})
	it('Alteração de produto-Item quantidade- Status 201', done => {
		request
			.put('/name/produtoTeste2')
			.send(dataProdutos.altProdutoPut)
			.expect(201)
			.end((err, res) => {
				expect(res.body.name).to.equal(dataProdutos.altProdutoPut.name)
				!expect(res.body.quantity).to.equal(dataProdutos.altProdutoPut.quantity)
				done(err)
			})
	})
})
