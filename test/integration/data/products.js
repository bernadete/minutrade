/*global module*/
/*eslint no-undef: "error"*/

const novoProduto = {
	name: 'produtoTeste1',
	value: 10,
	quantity: 20
}

const novoProdutoPut = {
	name: 'produtoTeste2',
	value: 10,
	quantity: 20
}

const altProdutoPut = {
	name: 'produtoTeste2',
	value: 10,
	quantity: 100
}

const novoProdutoDel = {
	name: 'produtoTeste3',
	value: 10,
	quantity: 20
}

const novoProdutoSchema = {
	name: 'produtoTeste4',
	value: 10,
	quantity: 20
}

const produtoSemNome = {
	value: 10,
	quantity: 20
}

module.exports = {
	novoProduto,
	novoProdutoPut,
	altProdutoPut,
	novoProdutoDel,
	novoProdutoSchema,
	produtoSemNome
}
