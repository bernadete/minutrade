const Joi = require('joi')


const schemaPayload = Joi.object().keys({
	name: Joi.string().alphanum().min(0).max(50).required(),
	value: Joi.number(),
	quantity: Joi.number(),
})

module.exports = {
	schemaPayload
}
