

let products = [
	{ name: 'cachorro', value: 100, quantity: 10 },
	{ name: 'gato', value: 50, quantity: 20 },
	{ name: 'galinha', value: 25, quantity: 50 }
]

const get = (req, res) => {
	res.status(200).send(products)
}

const post = (req, res) => {
	const newProduct = {
		name: req.body.name,
		value: Number.parseInt(req.body.value, 10),
		quantity: Number.parseInt(req.body.quantity, 10)
	}
	products.push(newProduct)
	res.status(201).send(newProduct)
}

const put = (req, res) => {
	const productName = req.params.name
	let product = products.find(item => (item.name === productName))
	product.value = Number.parseInt(req.body.value, 10)
	product.quantity = Number.parseInt(req.body.value, 10)
	res.status(201).send(product)
}

const del = (req, res) => {
	const productName = req.params.name
	let productIndex = products.findIndex(item => (item.name === productName))
	products.splice(productIndex, 1)
	res.status(201).send()
}

module.exports = {
	get,
	post,
	put,
	del
}
